﻿DROP SEQUENCE playerid;
DROP SEQUENCE itemtypeid;
DROP SEQUENCE itemid;
CREATE SEQUENCE playerid AS int START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE itemtypeid AS int START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE itemid AS int START WITH 1 INCREMENT BY 1;

IF OBJECT_ID('conn', 'U') IS NOT NULL DROP TABLE conn;
IF OBJECT_ID('itemtype', 'U') IS NOT NULL DROP TABLE itemtype;
IF OBJECT_ID('player', 'U') IS NOT NULL DROP TABLE player;
IF OBJECT_ID('item', 'U') IS NOT NULL DROP TABLE item;

CREATE TABLE itemtype(
	itemtype_id		NUMERIC(2) NOT NULL,
	itemtype_name	VARCHAR(10) NOT NULL,
	CONSTRAINT itemtype_PRIMARY_KEY PRIMARY KEY(itemtype_id)
);



CREATE TABLE item(
	item_id			NUMERIC(2) NOT NULL,
	item_type		NUMERIC(2) NOT NULL,
	item_strength	NUMERIC(2),
	item_agility	NUMERIC(2),
	item_energy		NUMERIC(2),
	item_name		NUMERIC(2),
	CONSTRAINT item_PRIMARY_KEY PRIMARY KEY (item_id),
	CONSTRAINT item_FOREIGN_KEY FOREIGN KEY (item_type) REFERENCES itemtype(itemtype_id)
	);

CREATE TABLE player(
	player_id		NUMERIC(2) NOT NULL,
	player_name		VARCHAR(20),
	player_level	NUMERIC(2) NOT NULL,
	player_xp		NUMERIC(5) NOT NULL,
	CONSTRAINT player_PRIMARY_KEY PRIMARY KEY (player_id));
CREATE TABLE conn(
	conn_id			NUMERIC(2) NOT NULL,
	conn_player		NUMERIC(2) NOT NULL,
	conn_item		NUMERIC(2) NOT NULL,
	CONSTRAINT conn_BOSS_KEY FOREIGN KEY (conn_player) REFERENCES player (player_id),
	CONSTRAINT conn_FOREIGN_KEY FOREIGN KEY (conn_item) REFERENCES item(item_id),
	CONSTRAINT conn_PRIMARY_KEY PRIMARY KEY (conn_id)
);