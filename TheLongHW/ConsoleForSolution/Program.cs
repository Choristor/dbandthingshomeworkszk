﻿using BusinessLogic;
using Data;
using Repository;
using Repository.PlayerRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleForSolution {
	class Program {
		static void Main(string[] args) {
			RealLogic rl = new RealLogic();

			List<Player> lista = rl.GetPlayers().ToList();
			Console.WriteLine(lista.Count);
			Console.ReadLine();
		}
	}
}
