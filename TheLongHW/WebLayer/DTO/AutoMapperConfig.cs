﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebLayer.Models;

namespace WebLayer.DTO {
	public class AutoMapperConfig {

		public static IMapper GetMapper() {
			var config = new MapperConfiguration(cfg => {
				cfg.CreateMap<Player, Data.Player>().ReverseMap();
				cfg.CreateMap<Player, Models.PlayerModel>().ReverseMap();
				cfg.CreateMap<IQueryable<Data.Player>, List<PlayerModel>>().ReverseMap();
			});
			return config.CreateMapper();
		}
	}
}