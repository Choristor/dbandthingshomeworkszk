﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.DTO {
	public class Player {
		public decimal id { get; set; }
		public string pname { get; set; }
		public decimal plevel { get; set; }
		public decimal xp { get; set; }

	}
}