﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace WebLayer.App_Start {
	public class WebApiConfig {
		public static void Register(HttpConfiguration config) {
			config.MapHttpAttributeRoutes();//Attribútumok alapján való metódus definíciókat

			//config.Routes.MapHttpRoute(
			//	name: "DefaultApi",
			//	routeTemplate: "api/{controller}/{id}",
			//	defaults: new { id = RouteParameter.Optional }
			//	);//engedélyezés és szignatúra megadása

			config.Routes.MapHttpRoute(
				name: "Action api",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
				);

			config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));//Json alap kimeneti formátum

			config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();//CamelCase formátum használata ha infot kap
		}
	}
}