﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models {
	public class CrudModel {
		public List<PlayerModel> List { get; set; }
		public PlayerModel EditObject { get; set; }
	}
}