﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebLayer.Models {
	public class PlayerModel {
		[Required]
		[Display(Name = "Player id")]
		public int id { get; set; }
		[Required]
		[StringLength(16,MinimumLength = 8)]
		[Display(Name = "Player name")]
		public string name { get; set; }
		[Required]
		[Display(Name = "Player level")]
		public int level { get; set; }
		[Required]
		[Display(Name = "Player xp")]
		public int xp { get; set; }
	}
}