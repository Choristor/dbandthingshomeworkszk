﻿using AutoMapper;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebLayer.Models;

namespace WebLayer.Controllers
{
    public class CrudController : Controller
    {
		IMapper mapper;
		ILogic logic;
		CrudModel model;

		protected override void OnActionExecuting(ActionExecutingContext filterContext) {
			mapper = DTO.AutoMapperConfig.GetMapper();
			logic = new RealLogic();
			model = new CrudModel();
			var dblist = logic.GetPlayers();
			model.List = mapper.Map<IQueryable<Data.Player>, List<PlayerModel>>(dblist);
			base.OnActionExecuting(filterContext);
		}

		private PlayerModel getPlayerModel(int id) {
			Data.Player p = logic.GetOnePlayer(id);
			return Mapper.Map<Data.Player, Models.PlayerModel>(p);
		}

		// GET: Crud
		public ActionResult Index()
        {
			ViewData["TargetAction"] = "AddNew";
			return View("CrudIndex", model);
        }

		public ActionResult Details() {
			return View("CrudeDataSheet", model);
		}
		public ActionResult Edit(int id) {
			ViewData["TargetAction"] = "Edit";
			model.EditObject = getPlayerModel(id);
			return View("CrudIndex",model);
		}
		[HttpPost]
		public ActionResult Edit(PlayerModel model) {
			if(ModelState.IsValid && model != null) {
				logic.ModifyPlayer(model.id, model.name);
				TempData["result"] = "Edit OK";

			} else {
				TempData["result"] = "Edit Fail";
			}
			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult AddNew(PlayerModel model) {
			if(ModelState.IsValid && model != null) {
				Data.Player p = mapper.Map<Models.PlayerModel, Data.Player>(model);
				p.id = logic.GetNextPlayerId();
				logic.AddPlayer(p);
				TempData["result"] = "Add OK";
			} else {
				TempData["result"] = "Add Fail";
			}
			return RedirectToAction("Index");
		}

		public ActionResult Remove(int id) {
			try {
				logic.DelPlayer(id);
				TempData["result"] = "Del OK";
			} catch(DbUpdateException) {
				TempData["result"] = "Del Fail";
			} catch(ArgumentException) {
				TempData["result"] = "Del Fail";
			}
			return RedirectToAction("Index");
		}
    }
}