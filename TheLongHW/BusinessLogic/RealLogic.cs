﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Repository;
using Repository.PlayerRepo;

namespace BusinessLogic {
	public class RealLogic : ILogic {
		MyRepository Repo;

		public RealLogic() {
			PlayerInventoryEntities1 DE = new PlayerInventoryEntities1();
			PlayerEFRepository PE = new PlayerEFRepository(DE);
			Repo = new MyRepository(PE);
		}

		public RealLogic(MyRepository newRepo) {
			Repo = newRepo;
		}
		public void AddPlayer(Player newplayer) {
			Repo.playerRepo.Insert(newplayer);
		}
		

		public void DelPlayer(int id) {
			Repo.playerRepo.Delete(id);
		}

		public int GetNextPlayerId() {
			return Repo.playerRepo.GetAll().Max(x => ((int)x.id)) + 1;
		}

		public Player GetOnePlayer(int id) {
			return Repo.playerRepo.GetById(id);
		}

		public IQueryable<Player> GetPlayers() {
			return Repo.playerRepo.GetAll().AsQueryable();
			
		}

		public void ModifyPlayer(int id, string newname) {
			Repo.playerRepo.Modify(id, newname);
		}
	}
}
