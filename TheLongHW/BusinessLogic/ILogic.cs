﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface ILogic
    {
		IQueryable<Player> GetPlayers();
		Player GetOnePlayer(int id);
		void AddPlayer(Player newplayer);
		void DelPlayer(int id);
		void ModifyPlayer(int id, string name);
		int GetNextPlayerId();
    }
}
