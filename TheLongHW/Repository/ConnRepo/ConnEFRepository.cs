﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.ConnRepo {
	public class ConnEFRepository : EFRepository<Conn> {
		public ConnEFRepository(DbContext newcontext) : base(newcontext) {
		}

		public override Conn GetById(int id) {
			return Get(akt => akt.id == id).SingleOrDefault();
		}
	}
}
