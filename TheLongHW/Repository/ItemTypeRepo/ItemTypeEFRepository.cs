﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.ItemTypeRepo {
	public class ItemTypeEFRepository : EFRepository<Itemtype> {
		public ItemTypeEFRepository(DbContext newcontext) : base(newcontext) {
		}

		public override Itemtype GetById(int id) {
			return Get(akt => akt.id == id).SingleOrDefault();
		}
	}
}
