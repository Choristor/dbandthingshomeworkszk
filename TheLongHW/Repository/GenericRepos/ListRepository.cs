﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos {
	abstract public class ListRepository<TEntity> : IRepository<TEntity>
		where TEntity : class {
		protected List<TEntity> list;
		public ListRepository(params TEntity[] entries) {
			list = new List<TEntity>();
			list.AddRange(entries);//A lista végéhez egy másik collectionböl ad hozzá elemeket
		}
		public void Delete(int id) {
			list.RemoveAt(id);
		}

		public void Delete(TEntity oldentity) {
			list.Remove(oldentity);
		}

		public void Dispose() {
			list.Clear(); //lista kiürítése
			list = null;
		}

		public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition) {
			return GetAll().Where(condition);
		}

		public IQueryable<TEntity> GetAll() {
			return list.AsQueryable();
		}

		public TEntity GetById(int id) {
			return list.ElementAt(id);
		}

		public void Insert(TEntity newentity) {
			throw new NotImplementedException();
		}
	}
}
