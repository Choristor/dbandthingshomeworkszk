﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.GenericRepos {
	abstract public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class {
		protected DbContext context;

		public EFRepository(DbContext newcontext) {
			context = newcontext;
		}
		public void Delete(int id) {
			TEntity oldentity = GetById(id);
			if(oldentity == null) {
				throw new ArgumentException("NO DATA");
			}
			Delete(oldentity);
		}

		public void Delete(TEntity oldentity) {
			context.Set<TEntity>().Remove(oldentity);//kikeresi az adatbázisból amit megadunk neki és törli
			context.SaveChanges();//lementi a változtatásokat
		}

		public void Dispose() {
			context.Dispose();
		}

		public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition) {
			return GetAll().Where(condition);
		}

		public IQueryable<TEntity> GetAll() {
			return context.Set<TEntity>();//vissza adja az egész adatbázis halmazát(itt a set halmazt jelent)
		}

		public abstract TEntity GetById(int id);
		public void Insert(TEntity newentity) {
			context.Set<TEntity>().Add(newentity);
			context.SaveChanges();
		}
	}
}
