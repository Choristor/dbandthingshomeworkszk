﻿using Repository.ConnRepo;
using Repository.ItemRepo;
using Repository.ItemTypeRepo;
using Repository.PlayerRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Repository {
	public class MyRepository {

		public IPlayerRepository playerRepo { get; private set; }
		public IItemRepository itemRepo { get; private set; }
		public IItemTypeRepository itemTypeRepo { get; private set; }

		public IConnRepository connRepo { get; private set; }

		public MyRepository(IPlayerRepository pr,IItemRepository ir,IItemTypeRepository itr, IConnRepository cr) {
			playerRepo = pr;
			itemRepo = ir;
			itemTypeRepo = itr;
			connRepo = cr;
		}

		public MyRepository(IPlayerRepository pE) {
			playerRepo = pE;
		}
	}
}
