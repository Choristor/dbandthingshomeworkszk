﻿using System;
using Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.GenericRepos;
namespace Repository.ItemRepo {
	public interface IItemRepository : IRepository<Item> {
	}
}
