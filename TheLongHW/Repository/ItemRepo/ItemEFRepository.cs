﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.ItemRepo {
	public class ItemEFRepository : EFRepository<Item> {
		public ItemEFRepository(DbContext newcontext) : base(newcontext) {
		}

		public override Item GetById(int id) {
			return Get(akt => akt.id == id).SingleOrDefault();
		}
	}
}
