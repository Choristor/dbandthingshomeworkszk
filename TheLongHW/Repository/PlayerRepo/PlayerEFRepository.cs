﻿using Data;
using Repository.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Repository.PlayerRepo {
	public class PlayerEFRepository : EFRepository<Player>,IPlayerRepository {
		public PlayerEFRepository(DbContext newcontext) : base(newcontext) {
		}

		public override Player GetById(int id) {
			return Get(akt => akt.id == id).SingleOrDefault();
		}

		public void Modify(int id, string newname) {
			Player akt = GetById(id);
			if(akt == null) {
				throw new InvalidOperationException();
			}
			if(newname != null) {
				akt.pname = newname;
			}
			context.SaveChanges();
		}
	}
}
